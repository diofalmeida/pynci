"""
    PyNCI: a python module for NCI index analysis of electronic densities.

    Copyright (C) 2020  Diogo A. F. Almeida

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Program usage.
    
    This version is dependent on the Atomic Simulation Environment (ASE)
    Python package. A working example is provided in workeg.py. For more
    information check description of function nci_analysis.

"""

__author__ = "Diogo A. F. Almeida"
__copyright__ = "Copyright (C) 2020 Diogo A. F. Almeida"
__license__ = "GPL"
__version__ = "0.3.1"
__maintainer__ = "Diogo Almeida"
__email__ = "diogo.almeida@student.fisica.uc.pt"
__status__ = "Development"
__credits__ = ["Diogo Almeida", "Bruce F. Milne"]

import numpy as np
import matplotlib.pyplot as plt

from ase import Atoms
from ase.io import write as writefunc
from ase.units import Bohr

Cs = 0.5*(3*(np.pi**2))**(-1.0/3) #a.u. constant for RDG computation

# --- #

def get_slices (axis, start, stop, step, ndim = 3):
	"""
	For use in 'derivate' function.
	"""
	
	slice_list = ndim*[slice(None, None, None),]
	slice_list[axis] = slice(start, stop, step)
	
	return tuple(slice_list)

def extend_array (fgrid, axis, pbc = False):
	"""
	For use in 'derivate' function.
	"""
	
	ndims = len(fgrid.shape)
	Iint = np.identity(ndims, dtype = int)
	
	extended_array = np.empty(np.array(fgrid.shape) + 2*Iint[axis], dtype = float)
	extended_array[get_slices(axis, 1, -1, None)] = np.array(fgrid)
	
	first_slices = get_slices(axis, 0, 1, None)
	last_slices = get_slices(axis, -1, None, None)
	
	if pbc:
		extended_array[first_slices] = fgrid[last_slices]
		extended_array[last_slices] = fgrid[first_slices]
	else:
		extended_array[first_slices] = 2*fgrid[first_slices] - fgrid[get_slices(axis, 1, 2, None)]
		extended_array[last_slices] = 2*fgrid[last_slices] - fgrid[get_slices(axis, -2, -1, None)]
	
	return extended_array

def derivate (fgrid, axis, spacing, pbc = False):
	"""
	Take 3D-array 'fgrid' and return derivative in direction 'axis' (= 0, 1, 2)
	using middle point finite difference method with spacing 'spacing'; pbc =
	periodic boundary conditions (True, False)
	
	Example: xgradient = derivate(density, 0, 0.1)
	"""
	
	new_fgrid = extend_array(fgrid, axis, pbc)
	
	fderiv = (0.5/spacing)*(new_fgrid[get_slices(axis, 2, None, None)] -\
							new_fgrid[get_slices(axis, 0, -2, None)])
	
	return fderiv
	
def gradient (fgrid, spacings, pbc = False):
	"""
	Take 3D-array 'fgrid' and return 4D array with 3 components along axis 0,
	pertaining to the three components of the gradient - check derivate;
	'spacings' can be a float or an array with the spacings in each direction;
	'pbc' can be a bool or an array with the conditions in each direction
	(e.g. [True True False]).
	"""
	
	ndims = len(fgrid.shape)
	
	if type(pbc) == bool:
		pbc = np.array(ndims*[pbc,])
	else:
		pbc = np.array(pbc)
	
	if type(spacings) == float:
		spacings = np.array(ndims*[spacings,])
	else:
		spacings = np.array(spacings)
	
	dimensions = np.empty(ndims + 1, dtype = int)
	dimensions[0] = ndims
	dimensions[1:] = fgrid.shape
	
	grad_array = np.empty(dimensions, dtype = float)
	for axis in range(ndims):
		grad_array[axis] = derivate(fgrid, axis, spacings[axis], pbc[axis])
	
	return grad_array

def fieldnorm (vector_field):
	"""
	Return array with norm of vector components along axis 0 in input 4D array.
	For use with output from gradient function to return gradient norm."
	"""
	
	vf2 = np.sum(vector_field**2, axis = 0)
	return np.sqrt(vf2)

def get_multidim_slices (ndims, slice_main, slice_ext, axis):
	"""
	Similar to get_slices. For use when dealing with multi(>3)-dimensional arrays
	in which we wish to slice only in the 3 last axes, and there exists a symmetry
	between two of these axes. Return tuple of slices.
	"""
	
	real_axis = ndims + axis - 3
	
	slice_list = ndims*[slice(None, None, None),]
	
	start, stop, step = slice_main
	slice_list[real_axis] = slice(start, stop, step)
	
	axis_ext_1 = ndims + ((axis + 1) % 3) - 3
	axis_ext_2 = ndims + ((axis + 2) % 3) - 3
	
	start, stop, step = slice_ext
	slice_list[axis_ext_1] = slice(start, stop, step)
	slice_list[axis_ext_2] = slice(start, stop, step)
	
	return tuple(slice_list)

def trilinear_grid_refiner (fgrid, pbc = False):
	"""
	Take array 'fgrid' with shape (..., nx, ny, nz) and perform trilinear
	interpolation into new array with half the spacing in each direction
	(of the last 3 axes); return new array with shape (..., 2*nx - 1,
	2*ny - 1, 2*nz - 1). All other axes remain unchanged.
	"""
	
	if type(pbc) == bool:
		pbc = np.array([pbc, pbc, pbc])
	
	oldgrid = np.array(fgrid)
	
	ndims = len(fgrid.shape)
	
	Iint = np.identity(ndims, dtype = int)[(ndims - 3):]
	
	for axis in range(3):
		if pbc[axis]:
			tmp_array = np.empty(np.array(oldgrid.shape) + Iint[axis])
			
			core_slices = get_multidim_slices(ndims, (None, -1, None), (None, None, None), axis)
			tmp_array[core_slices] = np.array(oldgrid)
			
			first_slices = get_multidim_slices(ndims, (0, 1, None), (None, None, None), axis)
			last_slices = get_multidim_slices(ndims, (-1, None, None), (None, None, None), axis)
			
			tmp_array[last_slices] = oldgrid[first_slices]
			
			oldgrid = np.array(tmp_array)
	
	
	newshape = 2*np.array(oldgrid.shape) - 1
	newshape[:-3] = oldgrid.shape[:-3]
	
	fgnew = np.empty(newshape, dtype = float)
	
	# --- #
	
	#first sweep: original points
	twos_slices = get_multidim_slices(ndims, (None, None, 2), (None, None, 2), 0)
	fgnew[twos_slices] = np.array(oldgrid)
	
	# --- #
	
	#second sweep: first sides
	fghnew = 0.5*fgnew
	
	for axis in range(3):
		new_slices = get_multidim_slices(ndims, (1, -1, 2), (None, None, 2), axis)
		old_slices_1 = get_multidim_slices(ndims, (None, -2, 2), (None, None, 2), axis)
		old_slices_2 = get_multidim_slices(ndims, (2, None, 2), (None, None, 2), axis)
		
		fgnew[new_slices] = fghnew[old_slices_1] + fghnew[old_slices_2]
	
	# --- #
	
	#third sweep: faces
	fghnew = 0.5*fgnew
	
	for axis in range(3):
		new_slices = get_multidim_slices(ndims, (None, None, 2), (1, -1, 2), axis)
		
		old_slices = list(new_slices) #right index
		left_index = ndims - 3 + ((axis + 2) % 3)
		
		old_slices[left_index] = slice(None, -2, 2) #left index of 1
		old_slices_1 = tuple(old_slices)
		
		old_slices[left_index] = slice(2, None, 2) #left index of 2
		old_slices_2 = tuple(old_slices)
		
		fgnew[new_slices] = fghnew[old_slices_1] + fghnew[old_slices_2]
	
	# --- #
	
	#fourth sweep: centers
	fghnew = 0.5*fgnew
	
	new_slices = get_multidim_slices(ndims, (1, -1, 2), (1, -1, 2), 0)
	old_slices_1 = get_multidim_slices(ndims, (None, -2, 2), (1, -1, 2), 0)
	old_slices_2 = get_multidim_slices(ndims, (2, None, 2), (1, -1, 2), 0)
	
	fgnew[new_slices] = fghnew[old_slices_1] + fghnew[old_slices_2]
	
	# --- #
	
	for axis in range(3):
		if pbc[axis]:
			core_slices = get_multidim_slices(ndims, (None, -1, None), (None, None, None), axis)
			fgnew = np.array(fgnew[core_slices])
	
	return fgnew

def compute_hessian (fgrid, spacings, pbc = True):
	"""
	Compute the 6 independent elements of the Hessian matrix.
	
	Inputs: fgrid - array of the function to derivate; spacings - either a
	float or a 1D-array (or equivalent) of the grid spacings in each direction;
	pbc - periodic border conditions in each direction.
	
	Output: 4D array with 6 components along axis 0 corresponding to the independent
	elements of the Hessian matrix at each point, in the following order: 11, 22, 33,
	12, 13, 23. Derivation is done using center based finite difference.
	
	Example: hessian_matrix = compute_hessian(density, [0.1 0.1 0.1])
	"""
	
	if type(pbc) == bool:
		pbc = np.array([pbc, pbc, pbc])

	fgrid_shape = np.array(fgrid.shape)
	
	dimensions = np.empty(4, dtype = int)
	
	dimensions[0] = 6
	dimensions[1:] = fgrid_shape
	
	if type(spacings) in (float, int):
		spacings = np.array([spacings, spacings, spacings])
	else:
		spacings = np.array(spacings)
	
	prefacs = spacings**(-2)
	
	#initialize 6 independent elements:
	Hessian = np.zeros(dimensions)
	
	#compute diagonal elements:
	for axis in range(3):
		new_fgrid = extend_array(fgrid, axis, pbc[axis])
		Hessian[axis] = prefacs[axis]*(new_fgrid[get_slices(axis, 2, None, None)] -\
									   2*new_fgrid[get_slices(axis, 1, -1, None)] +\
									   new_fgrid[get_slices(axis, None, -2, None)])
	
	#compute off-diagonal elements:
	grad_array = gradient(fgrid, spacings, pbc)
	
	for n in range(3, 6):
		anti_axis = 5 - n
		axis_1 = (anti_axis + 1) % 3
		axis_2 = (anti_axis + 2) % 3
		
		Hessian[n] = derivate(grad_array[axis_1], axis_2, spacings[axis_2], pbc[axis_2])
	
	return Hessian

def real_space_hessian (hessian_matrix, chi_matrix):
	"""
	Return real space hessian matrix from fractional coordinates hessian
	and inverse of cell matrix.
	"""
	
	hess_indices = [(0, 0), (1, 1), (2, 2), (0, 1), (0, 2), (1, 2)]
	
	real_hess = np.empty(hessian_matrix.shape)
	for n in range(6):
		real_hess[n] = 0
		alpha, gamma = hess_indices[n]
		for beta in range(3):
			diag = chi_matrix[alpha, beta]*chi_matrix[gamma, beta]*hessian_matrix[beta]
			
			del1, del2 = hess_indices[5 - beta]
			
			#original:
#			ndig = chi_matrix[alpha, del1]*chi_matrix[gamma, del2]*hessian_matrix[5 - beta]
#			
#			real_hess[n] = real_hess[n] + diag + 2*ndig
			#end original
			
			#attempted correction:
			ndig = (chi_matrix[alpha, del1]*chi_matrix[gamma, del2] +\
					chi_matrix[gamma, del1]*chi_matrix[alpha, del2])*hessian_matrix[5 - beta]
			
			real_hess[n] = real_hess[n] + diag + ndig
			#end attempted correction - this worked
	
	return real_hess

def dim4_separate (dim4_array):
	"""
	Take 4D array 'dim4_array' with n components (along axis 0) and return tuple
	of individual components as 3D arrays. Opposite of dim4_join.
	"""
	
	ncomponents = dim4_array.shape[0]
	output = ncomponents*[0,]
	for n in range(ncomponents):
		output[n] = dim4_array[n]
	
	return tuple(output)

def dim4_join (dim3_arrays):
	"""
	Take tuple of 3D arrays 'dim3_arrays' and return single 4D array with n =
	len(dim3_arrays) components along axis 0. Opposite of dim4_separate. All
	3D arrays must have same shape.
	"""
	
	ncomponents = len(dim3_arrays)
	
	dimensions = np.empty(4, dtype = int)
	dimensions[0] = ncomponents
	dimensions[1:] = dim3_arrays[0].shape
	
	output = np.empty(dimensions, dtype = float)
	for n in range(ncomponents):
		output[n] = dim3_arrays[n]
	
	return output
	
def hessian_coefficients (hessian_matrix):
	"""
	Input: single 4D array with 6 components along axis 0 corresponding to the
	independent components of the Hessian matrix of the density (in the specified
	order - ij = 11, 22, 33, 12, 13, 23 -- direct output of the compute_hessian
	function)
	
	Output: coefficients (in array form) of the characterstic polynomial of H:
	tau(trace), beta, delta(determinant).
	P(x) = -x^3 + tau*(x^2) - beta*x + delta
	"""
	
	H11, H22, H33, H12, H13, H23 = dim4_separate(hessian_matrix)
	
	tau = H11 + H22 + H33
	delta = H11*H22*H33 + 2*H12*H13*H23 - (H11*(H23**2) + H22*(H13**2) + H33*(H12**2))
	beta = H11*H22 + H22*H33 + H11*H33 - (H12**2 + H23**2 + H13**2)
	
	return tau, beta, delta

def IDfunc (x):
	"""For use when defining iterator. Take x and return x."""
	
	return x

def evaluate_sign_eigen (hessian_matrix, show_progress = False):
	"""
	Input: coefficients of the characteristic polynomial of the Hessian matrix
	of the density - tau, beta, delta (direct output of the hessian_coefficients
	function); show_progress - enable for showing progress bar (tqdm) during
	computation.
	
	Output: 3D array with the sign of the median eigenvalue of the Hessian matrix
	at each grid point.
	"""
	
	tau, beta, delta = hessian_coefficients(hessian_matrix)
	
	if show_progress:
		from tqdm import tqdm
		iterator = tqdm
	else:
		iterator = IDfunc
	
	tauprime = tau/3.0
	difprime = np.sqrt((tauprime**2) - beta/3.0)

	inflex1grid = tauprime - difprime
	inflex2grid = tauprime + difprime
	
	#initialize sign array and auxilliary arrays:
	sgnl2 = np.empty(tau.shape, dtype = int)
	checkX1 = np.empty(tau.shape, dtype = bool)
	checkX2 = np.empty(tau.shape, dtype = bool)
	zero = np.zeros(tau.shape)
	
	Pchar1grid = inflex1grid**3 + tau*(inflex1grid**2) - beta*inflex1grid + delta
	Pchar2grid = inflex2grid**3 + tau*(inflex2grid**2) - beta*inflex2grid + delta
	
	checkX1 = (inflex1grid > zero)
	checkX2 = (inflex2grid < zero)
	
	checkP1 = np.equal(Pchar1grid, zero)
	checkP2 = np.equal(Pchar2grid, zero)
	
	sign_of_inflex1 = np.sign(inflex1grid)
	sign_of_inflex2 = np.sign(inflex2grid)
	sign_of_delta = np.sign(delta)
	
	Nx, Ny, Nz = tau.shape
	
	#sweep array:
	for i in iterator(range(Nx)):
		for j in range(Ny):
			for k in range(Nz):
				pos = (i, j, k)
				
				if checkX1[pos]:
					sgnl2[pos] = 1
					
				elif checkX2[pos]:
					sgnl2[pos] = -1
					
				else:
					if checkP1[pos]:
						sgnl2[pos] = sign_of_inflex1[pos]
						
					elif checkP2[pos]:
						sgnl2[pos] = sign_of_inflex2[pos]
					
					else:
						sgnl2[pos] = -sign_of_delta[pos]
		
	return sgnl2

def get_grid (denshape, origin, cell_matrix):
	"""
	Return real grid (4D) in Angstrom, X[alpha, i, j, k]
	where alpha relates to component (0,1,2 = x,y,z) and (ijk) to grid position.
	"""
	
	grid_shape = np.empty(4, dtype = int)
	grid_shape[0] = 3
	grid_shape[1:] = denshape
	
	index_grid = np.empty(grid_shape, dtype = int)
	
	index_list = []
	for n in range(3):
		in_grid = np.array(range(denshape[n]), dtype = int)
		index_list.append(in_grid)
	
	new_index_list = np.meshgrid(index_list[0], index_list[1], index_list[2], indexing = 'ij')
	
	fractional_grid = np.empty(grid_shape)
	for n in range(3):
		fractional_grid[n] = new_index_list[n]*1.0/denshape[n]
	
	# --- #
	
	real_grid = np.zeros(grid_shape)
	
	for n in range(3):
		real_grid[n] = real_grid[n] + origin[n]
		for m in range(3):
			real_grid[n] = real_grid[n] + cell_matrix[m, n]*fractional_grid[m]
	
	# --- #
	
	return real_grid

def grid_distances (denshape, origin, atoms_object):
	"""
	Return 4D array with distances (in Angstrom) to each nuclear position
	(axis 0 specifies to which atom).
	"""
	
	cell_matrix = atoms_object.get_cell()
	atom_positions = atoms_object.get_positions()
	
	real_grid = get_grid(denshape, origin, cell_matrix)
	
	dis_shape = np.empty(5, dtype = int)
	dis_shape[0] = len(atoms_object)
	dis_shape[1] = 3
	dis_shape[2:] = denshape

	displacements = np.empty(dis_shape, dtype = float)

	for alpha in range(dis_shape[0]):
		for beta in range(3):
			displacements[alpha, beta] = real_grid[beta] - atom_positions[alpha, beta]

	distances = np.sqrt(np.sum(displacements**2, axis = 1))
	
	return distances

def check_psp_cutoffs (denshape, origin, atoms_object, psp_cutoffs):
	"""
	Return 3D array of Booleans: True if grid point is `outside' cutoff radii and
	False if `inside'.
	
	psp_cutoffs is a dictionary of type {atomic symbol: cutoff radius (in Angstrom)}
	"""
	
	distances = grid_distances(denshape, origin, atoms_object)
	symbols = atoms_object.get_chemical_symbols()
	
	out_of_bounds = np.full(denshape, True, dtype = bool)
	
	for alpha in range(len(atoms_object)):
		atom_check = distances[alpha] > psp_cutoffs[symbols[alpha]]
		
		out_of_bounds = (atom_check & out_of_bounds)
	
	return out_of_bounds

# --- #

def nci_analysis (density, atoms_object, origin = np.array([0, 0, 0]),\
				  slicing_input = None, system_name = 'systemdefault',\
				  scatterplot_cutoffs = (4e-2, 4e-2), gridinterpol = False,\
				  show_progress = False, show_figures = False, psp_cutoffs = None,\
				  rdg_correction_order = 3, trim_scatter = True):
	"""
	Tool to perform real space finite-difference based NCI index analysis.
	
	Input:
	* density - 3D density array (must be converted to a.u. before being used as
		input, if necessary);
	* atoms_object - ASE Atoms object;
	* origin - array containing the coordinates of the origin for the exported
		cube files (in Angstrom, in order to coincide with the origin returned by
		the ASE cube reader);
	* slicing_input - reccomended in finite systems in which we intend to study a
		particular portion of space - should be a 2x3 array where the first line has
		the coordinates of the new origin of the array, and the second line has the
		coordinates of the opposite corner - coordinates should be in Angstrom for
		finite systems and in fractional coordinates for solids (the tool
		automatically distinguishes the two types by checking if the cell matrix
		is diagonal);
	* system_name - string containing the name of the system in question - will be
		used in exported cube, text and image files in the form
		('[object]_nci_%s.[format]' % system_name);
	* scatterplot_cutoffs - defines the negative and positive (in this order) cutoffs
		for the values of (sign(lambda2) x density) in the exported cube files
		(*does not* affect scatterplot, only the cube files);
	* gridinterpol - if False or 0 does nothing, otherwise performs the number p of
		trilinear interpolations specified, so that the spacings in the final grids
		are h' = h x 2^(-p);
	* show_progress - option to show tqdm progress bars during calculations
		that involve sweeps of the grid;
	* show_figures - specify whether to show the scatterplot at the end of the
		analysis in the matplotlib GUI;
	* psp_cutoffs - dictionary containing {chemical symbol: highest psp cutoff radius
		(in Angstrom)} for pseudo-densities - removes points from scatterplot
		corresponding to regions too close to nuclei positions (*does not* affect cube
		files, only scatterplot);
	* rdg_correction_order - order of Taylor-series-based correction of reduced
		density gradient due to small shift in density.
	* trim_scatter - option to remove points from scatterplot data output that do not
		appear in usual scatterplot image (density < 0.075 a.u.)
	
	Besides exporting relevant image, cube and text files, returns a two collumn
	array with the coordinates of the scatterplot.
	
	Keep in mind that this function may alter the inputs, use only at the end of
	calculations that involve the inputs.
	"""
	
	if show_progress:
		from tqdm import tqdm
		iterator = tqdm
	else:
		iterator = IDfunc
	
	# --- #
	
	print("Performing NCI real space finite-difference based analysis:")
	
	# --- # input 'sanitation': check negative densities
	
	density_not_negative = (density >= 0.0).astype(int)
	density = density*density_not_negative
	
	# --- #
	
	use_pbc = atoms_object.get_pbc()
	print("\n>> Border conditions: %s" % use_pbc)
	
	cell_matrix = np.array(atoms_object.get_cell())/Bohr
	print("\n>> Cell matrix:")
	print(cell_matrix)
	
	denshape = np.array(density.shape)
	
	#check if cell_matrix is diagonal:
	solid_sys = bool(np.count_nonzero(cell_matrix - np.diag(np.diagonal(cell_matrix))))
	
	if solid_sys:
		print("\n>> Solid unit cell detected.")
		
		print(" > Computing inverse of cell matrix...")
		chi_matrix = np.linalg.inv(cell_matrix)
		print(" > Done. Inverse is")
		print(chi_matrix)
		
		spacings = 1.0/(denshape - 1)
	
	else:
		print("\n>> Finite system (or orthogonal cell) detected.")
		
		lenghts = np.diagonal(cell_matrix)
		spacings = lenghts/denshape
		print(" > Using spacings %s" % spacings)
		
	# --- #
	
	print("\n>> Computing derivatives...")
	grad_vecfield = gradient(density, spacings, pbc = use_pbc)
	
	if solid_sys:
		print(" > Reverting gradient to real space...")
		tmp_array = np.empty(grad_vecfield.shape, dtype = float)
		
		for axis_1 in range(3):
			tmp_array[axis_1] = 0
			for axis_2 in range(3):
				tmp_array[axis_1] = tmp_array[axis_1] +\
									chi_matrix[axis_1, axis_2]*grad_vecfield[axis_2]

		grad_vecfield = np.array(tmp_array)
		
	rdg_prefac = Cs*fieldnorm(grad_vecfield)
	
	print(" > RDG computation...")
	
	#shift factors:
	shift_amplitude = 1e-16
	shift_length = shift_amplitude
	
	delta_rho = shift_amplitude*np.exp(-(density/shift_length)**2)

	fake_density = density + delta_rho
	
	RDGgrid = rdg_prefac/(fake_density**(4.0/3))
	
	# --- # correction terms:
	if rdg_correction_order:
		eta_factor = delta_rho*1.0/fake_density
		gamma = 1.0
		rdg_factor = 1.0
		
		for k in range(rdg_correction_order):
			gamma = gamma*(k + 4.0/3)/(k + 1)
			rdg_factor = rdg_factor + gamma*(eta_factor**(k + 1))
		
		RDGgrid = RDGgrid*rdg_factor
		
		ave_corr_den = np.sum(rdg_factor - 1.0)/density.size
		print("  - average correction density: %.3e (a.u.)" % ave_corr_den)
	
	# --- # end correction terms
	
	print(" > Hessian matrix computation...")
	hessian_matrix = compute_hessian(density, spacings, pbc = use_pbc)
	
	if solid_sys:
		print(" > Reverting Hessian to real space...")
		hessian_matrix = real_space_hessian(hessian_matrix, chi_matrix)
	
	print(" > Done.")
	
	# --- #
	
	if slicing_input is not None:
		print("\n>> Cutting down region of interest...")
		if solid_sys:
			slicing_spacings = np.array(spacings)
		else:
			slicing_spacings = spacings*Bohr
		
		origin_gridpoint = ((slicing_input[0] - origin)*1.0/slicing_spacings + 0.5).astype(int)
		corner_gridpoint = ((slicing_input[1] - origin)*1.0/slicing_spacings + 0.5).astype(int)
		
		iorg, jorg, korg = tuple(origin_gridpoint)
		icor, jcor, kcor = tuple(corner_gridpoint)
		
		normal_slices = (slice(iorg, icor+1, None),\
						 slice(jorg, jcor+1, None),\
						 slice(korg, kcor+1, None))
		
		density = density[normal_slices]
		RDGgrid = RDGgrid[normal_slices]
		
		hessian_slices = 4*[slice(None, None, None),]
		hessian_slices[1:] = normal_slices
		
		hessian_matrix = hessian_matrix[tuple(hessian_slices)]
		
		# --- #
		
		ratios = (corner_gridpoint - origin_gridpoint + 1)*1.0/denshape
		
		new_cell = np.array(cell_matrix)*Bohr
		new_cell = new_cell*ratios
		
		atoms_object.set_cell(new_cell)
		
		denshape = np.array(density.shape)
		
		origin = origin + origin_gridpoint*slicing_spacings
		if solid_sys:
			origin = np.matmul(origin, cell_matrix)*Bohr
		
		print(" > Done.")
	
	# --- #
	
	#perform trilinear interpolation if necessary
	if gridinterpol:
		if slicing_input is None:
			interpol_pbc = use_pbc
		else:
			interpol_pbc = False
		
		ordinals_list = (0, "first", "second", "third")
	
		print("\n>> Interpolating results...")
		
		interpol_stage = 1
		while interpol_stage <= gridinterpol:
			if interpol_stage <= 3:
				print(" > performing %s refinement" % ordinals_list[interpol_stage])
			else:
				print(" > performing %ith refinement" % interpol_stage)

			interpolate_array = dim4_join((density, RDGgrid))
			interpolate_array = trilinear_grid_refiner(interpolate_array, pbc = interpol_pbc)
			density, RDGgrid = dim4_separate(interpolate_array)
			print ("  - density and RDG interpolated")
			
			hessian_matrix = trilinear_grid_refiner(hessian_matrix, pbc = interpol_pbc)
			print ("  - hessian matrix interpolated")
			
			spacings = 0.5*spacings
		
			interpol_stage += 1

		print(" > Done.")
	
	# --- #
	
	print("\n>> Median eigenvalue sweep...")
	
	sgnl2 = evaluate_sign_eigen(hessian_matrix, show_progress)
	
	print(" > Done.")
	
	denplt = sgnl2*density
	
	# --- #
	
	print("\n>> Sweeping array for scatterplot cutoffs...")
	
	denshape = density.shape
	cutoffplus = np.zeros(denshape) + scatterplot_cutoffs[1]
	cutoffmins = np.zeros(denshape) - scatterplot_cutoffs[0]
	cutoffrdg = np.zeros(denshape) + 2.0
	
	check_scatter_cutoffs = (denplt > cutoffmins) & (denplt < cutoffplus) & (RDGgrid < cutoffrdg)
	
	none_array = np.full(denshape, None, dtype = float)
	exported_denplt = np.where(check_scatter_cutoffs, denplt, none_array)
	exported_rdggrd = np.where(check_scatter_cutoffs, RDGgrid, none_array)
	
	print(" > Done.")
	
	# --- #
	
	print("\n>> Exporting cube files...")
	
	writefunc("rdg_nci_%s.cube" % system_name, atoms_object,\
			  format = 'cube', data = exported_rdggrd, origin = origin)
	writefunc("sgn2den_nci_%s.cube" % system_name, atoms_object,\
			  format = 'cube', data = exported_denplt, origin = origin)
	
	print(" > Done.")
	
	# --- #
	
	print("\n>> Plotting scatterplot...")
	
	denplt = denplt.reshape(denplt.size)
	rdgplt = RDGgrid.reshape(RDGgrid.size)
	
	if psp_cutoffs is not None:
		print(" > Removing points near nuclei from scatterplot...")
		
		out_of_bounds = check_psp_cutoffs(denshape, origin, atoms_object, psp_cutoffs)
		out_of_bounds = out_of_bounds.reshape(out_of_bounds.size)
	
		denplt = denplt[out_of_bounds]
		rdgplt = rdgplt[out_of_bounds]
	
	plt.figure('NCI scatterplot')
	
	plt.plot(denplt, rdgplt, '.', markersize = 1.0)
	plt.plot((0, 0), (0, 2), 'k--', linewidth = 0.75)
	
	plt.xlim([-0.075, 0.075])
	plt.ylim([0, 2])

	plt.xlabel(r"sign$(\lambda_2)\times \rho$ (a.u.)")
	plt.ylabel(r"$s$ (a.u.)")
	
	plt.savefig("scatterplot_nci_%s.png" % system_name, format = 'png',\
				bbox_inches = 'tight')
	
	print(" > Done.")
	
	# --- #
	
	print("\n>> Exporting image data...")
	
	datafile = open('scatterdata_nci_%s.dat' % system_name, 'w')
	
	datafile.write('# NCI real-space finite difference based analysis for %s\n' % system_name)
	datafile.write('# scatterplot data: sign(lambda2)*density vs reduced density gradient (a.u.)\n')
	datafile.write('#\n')
	datafile.write('#   rho*sgn(l2)   |       rdg      ')
	
	if trim_scatter:
		trim_condition = (denplt < 0.075) & (denplt > -0.075)
		
		denplt = denplt[trim_condition]
		rdgplt = rdgplt[trim_condition]
	
	for line in iterator(range(denplt.size)):
		datafile.write("\n%17.8e %16.8e" % (denplt[line], rdgplt[line]))
	
	datafile.close()
	
	scatterplot_output = np.empty((denplt.size, 2), dtype = float)
	scatterplot_output[:, 0] = denplt
	scatterplot_output[:, 1] = rdgplt
	
	print(" > Done.")
	
	# --- #
	
	print("\n>> All computations done.")
	
	if show_figures:
		plt.show()
	
	return scatterplot_output
