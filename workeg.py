import numpy as np

from ase import Atoms
from ase.io import write
from ase.units import Bohr

from gpaw import GPAW

#import nci_analysis tool:
from pynci import nci_analysis as ncitest

# --- # density computation of Zinc dimer with ASE and GPAW:

dis = 4.19

calc = GPAW(mode = 'lcao', basis = 'dzp', xc = 'PBE', h = 0.15, txt = 'Zn2_gpawcalc.log')

Zndimer = Atoms("Zn2", positions = [(0, 0, 0), (0, 0, dis)],\
				cell = [(7.5, 0, 0), (0, 7.5, 0), (0, 0, 20)],\
				pbc = False, calculator = calc)

Zndimer.center()

#defining slicing input to focus on interaction region:
cake = np.empty((2, 3))
cake[0] = Zndimer.get_positions()[0] - np.array([1.0, 1.0, 1.0])
cake[1] = Zndimer.get_positions()[1] + np.array([1.0, 1.0, 1.0])

write('Zn2.xyz', format = 'xyz', images = Zndimer)

# --- #

energy = Zndimer.get_potential_energy()

density = calc.get_all_electron_density(gridrefinement = 2)
density = density*(Bohr**3)

# --- # nci analysis with pynci:

scatter = ncitest(density,\
				  #^ density array
				  Zndimer,\
				  #^ ASE Atoms object
				  system_name = 'Zn2',\
				  #^ basename for files
				  slicing_input = cake,\
				  #^ slicing input
				  gridinterpol = 1,\
				  #^ grid interpolation level
				  show_progress = True,\
				  show_figures = True)
